from django.db import models

# Create your models here.
class DelinquencyInputs(models.Model):
    variable_name = models.CharField(max_length=200)
    target_indicator = models.BooleanField()
    def __str__(self):
        return f'variable_name: {self.variable_name}, target_indicator: {self.target_indicator}'

class DelinquencyModel(models.Model):
    model_name = models.CharField(max_length=50)
    algorithm_used = models.CharField(max_length=100)
    def __str__(self):
        return f'model_name: {self.model_name}, algorithm_used: {self.algorithm_used}'

class ModelAttributes(models.Model):
    delinquency_model = models.ForeignKey(DelinquencyModel, on_delete=models.CASCADE)
    attribute_name = models.CharField(max_length=100)
    attribute_value = models.CharField(max_length=100)
    def __str__(self):
        return f'Attribute name: {self.attribute_name}, Attribute value: {self.attribute_value}, Delinquency model: {self.delinquency_model}'