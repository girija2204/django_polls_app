from django.http import HttpResponse, Http404
from django.shortcuts import render, get_object_or_404, get_list_or_404

from .models import DelinquencyModel


# Create your views here.
def index(request):
    all_models = get_list_or_404(DelinquencyModel)
    context = {'all_models_list': all_models}
    return render(request, 'polls/index.html', context)


def getModelDetails(request):
    model_detail = get_object_or_404(DelinquencyModel, pk=request.POST['model'])
    return render(request, 'polls/detail.html', {'model_detail': model_detail})

def getTrainingResult(request, model_detail_id):
    model_detail = get_object_or_404(DelinquencyModel, pk=model_detail_id)
    print(model_detail)
    for attr in model_detail.modelattributes_set.all():
        print(attr)
    result = request.POST['xxx'] + request.POST['xxy']
    return HttpResponse("Training is done successfully"+result)