from django.urls import path

from . import views

app_name = 'polls'
urlpatterns = [
    path('', views.index, name='index'),
    path('model/', views.getModelDetails, name='detail'),
    path('model/<int:model_detail_id>/result', views.getTrainingResult, name='trainingResult')
]
